#include "produktni_graf.hpp"
#include "mcqd.hpp"
#include "debug.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include <ctime>

extern double maxr;
extern double s;

using namespace std;

Produktni_graf::Produktni_graf (const Molekula &m1, const Molekula &m2, string dim) {

	qmax = nullptr;
	if (dim == "3D")
		naredi_produktni_graf_3D (m1, m2, false);
	else if (dim == "2D")
		naredi_produktni_graf (m1, m2);
	else
		naredi_produktni_graf_3D (m1, m2, true);

	naredi_pMatriko ();

}

Produktni_graf::~Produktni_graf() {
	if (qmax != nullptr)
		delete[] qmax;
}


void Produktni_graf::naredi_produktni_graf (const Molekula &m1, const Molekula &m2) {

	for (int i = 0; i < (signed)m1.v.size(); i++) {
		for (int j = 0; j < (signed)m2.v.size(); j++) {
			if (m1.v[i].get_tip_atoma() == m2.v[j].get_tip_atoma()) {
				vozlisca.push_back (Komponente({i,j}));
			}
		}
	}

	for (int a = 0; a < (signed)vozlisca.size(); a++) {
		for (int b = 0; b < (signed)vozlisca.size(); b++) {
			if (vozlisca[a].komponenta_1 != vozlisca[b].komponenta_1 && vozlisca[a].komponenta_2 != vozlisca[b].komponenta_2) {
#ifdef _WIN32
				if (fabs(double(m1.get_dolzinska_matrika()[vozlisca[a].komponenta_1][vozlisca[b].komponenta_1] - m2.get_dolzinska_matrika()[vozlisca[a].komponenta_2][vozlisca[b].komponenta_2])) <= s) {
					vozlisca[a].pVezi.push_back(b);
					vozlisca[b].pVezi.push_back(a);
				}

#else
//				if (fabs( m1.get_dolzinska_matrika() [vozlisca[a].komponenta_1] [vozlisca[b].komponenta_1] - m2.get_dolzinska_matrika() [vozlisca[a].komponenta_2] [vozlisca[b].komponenta_2] == s) ) {
				if (fabs( m1.get_dolzinska_matrika() [vozlisca[a].komponenta_1] [vozlisca[b].komponenta_1] - m2.get_dolzinska_matrika() [vozlisca[a].komponenta_2] [vozlisca[b].komponenta_2]) <= s) {
					vozlisca[a].pVezi.push_back(b);
					vozlisca[b].pVezi.push_back(a);
				}
#endif
			}
		}
	}

}


void Produktni_graf::naredi_produktni_graf_3D (const Molekula &m1, const Molekula &m2, bool bio) {

	for (int i = 0; i < (signed)m1.v.size(); i++) {
		for (int j = 0; j < (signed)m2.v.size(); j++) {
			if ( m1.v[i].get_tip_atoma() == m2.v[j].get_tip_atoma()) { 
				vozlisca.push_back (Komponente({i,j}));
			}
			// za bio preveri, ce je seed od tarcne molekule v referencni molekuli
			else if (bio) {
				vector<string> compSeeds = m1.complementarySeeds.find(m1.v[i].get_tip_atoma()) -> second;
				if(find(compSeeds.begin(), compSeeds.end(), m2.v[j].get_tip_atoma()) != compSeeds.end()) {
					vozlisca.push_back (Komponente({i,j}));
				} 
			}
		}
	}

	int temp = 0;
	for (int a = 0; a < (signed)vozlisca.size(); a++) {
		for (int b = 0; b < (signed)vozlisca.size(); b++) {
			if (vozlisca[a].komponenta_1 != vozlisca[b].komponenta_1 && vozlisca[a].komponenta_2 != vozlisca[b].komponenta_2) {


				double kvadrat_razlike_a_x = pow ((m1.v[vozlisca[a].komponenta_1].get_x() - m1.v[vozlisca[b].komponenta_1].get_x()), 2);
				double kvadrat_razlike_a_y = pow ((m1.v[vozlisca[a].komponenta_1].get_y() - m1.v[vozlisca[b].komponenta_1].get_y()), 2);
				double kvadrat_razlike_a_z = pow ((m1.v[vozlisca[a].komponenta_1].get_z() - m1.v[vozlisca[b].komponenta_1].get_z()), 2);

				double kvadrat_razlike_b_x = pow ((m2.v[vozlisca[a].komponenta_2].get_x() - m2.v[vozlisca[b].komponenta_2].get_x()), 2);
				double kvadrat_razlike_b_y = pow ((m2.v[vozlisca[a].komponenta_2].get_y() - m2.v[vozlisca[b].komponenta_2].get_y()), 2);
				double kvadrat_razlike_b_z = pow ((m2.v[vozlisca[a].komponenta_2].get_z() - m2.v[vozlisca[b].komponenta_2].get_z()), 2);



				double razdalja_a = sqrt (kvadrat_razlike_a_x + kvadrat_razlike_a_y + kvadrat_razlike_a_z);
				double razdalja_b = sqrt (kvadrat_razlike_b_x + kvadrat_razlike_b_y + kvadrat_razlike_b_z);

				double R = abs (razdalja_a - razdalja_b);


				if (R < maxr) {
					temp++;
					vozlisca[a].pVezi.push_back(b);
					vozlisca[b].pVezi.push_back(a);
				}
			}
		}
	}

}

const int Produktni_graf::dobi_velikost_PG () {
	return vozlisca.size();
}

const vector <Komponente> &Produktni_graf::get_vozlisca () const{
	return vozlisca;
}

void Produktni_graf::naredi_pMatriko() {
	pMatrika.resize (vozlisca.size(), vector <bool> (vozlisca.size (), false));
	for (int i = 0; i < (signed)vozlisca.size (); i++) {
		for (int j = 0; j < (signed)get_vozlisca()[i].pVezi.size(); j++) {
			pMatrika [i] [vozlisca[i].pVezi[j]] = true;
		}
	}
}

const vector <vector <bool> > &Produktni_graf::get_pMatrika () const{
	return pMatrika;
}

string Produktni_graf::primerjaj (const Molekula &m1, const Molekula &m2, int writeMol) {

	const int sz = get_vozlisca().size();
	bool **vrstice = new bool* [sz] ;
	for (int i = 0; i < sz; i++) {
		vrstice[i] = new bool [sz];
		for (int j = 0; j < sz; j++) {
			vrstice[i][j] = get_pMatrika()[i][j];
			//~ dbgmsg("i = " << i << " j = " << j << " vrstice = " << vrstice[i][j]);
		}
	}
	dbgmsg("sz = " << sz);
	Maxclique m (vrstice, sz);
	dbgmsg("pred mcq");
	m.mcqdyn(qmax, qsize);
	//cout << *qmax << " " << qsize << endl;
	dbgmsg("po mcq");
	for (int i = 0; i < sz; i++) {
		delete[] vrstice[i];
	}
	delete[] vrstice;


	if (writeMol = 1) {
        string retString = "\n@<TRIPOS>COMMENT\n\n______________________________LiSiCA  RESULTS______________________________\n"
                "\nRef. Position\tRef. Atom\tTar. Position\tTar. Atom\tAtom Type"
                "\n--------------------------------------------------------------------------- \n";
        for (int i = 0; i < qsize; i++) {
           retString += to_string(vozlisca.at(qmax[i]).komponenta_1+1) + "\t\t" + m1.v[vozlisca.at(qmax[i]).komponenta_1].get_ime_atoma() + "\t"
           "\t" + to_string(vozlisca.at(qmax[i]).komponenta_2+1) + "\t\t" + m2.v[vozlisca.at(qmax[i]).komponenta_2].get_ime_atoma() + "\t"
           "\t"+ m1.v[vozlisca.at(qmax[i]).komponenta_1].get_tip_atoma() + "\n";
        }
        return retString;
	}
	else {
        return "";
	}


}





