#include <iostream>
#include <mutex>
#include <vector>
#include <memory>
#include <time.h>
#include <algorithm>
#include <map>
#include <sstream>

#include "molekula.hpp"

using namespace std;

extern ifstream infile;
extern bool vodiki;
extern bool bioisoster;
bool first = true;
int read_state;
int st_empty = 0;

Molekula::Molekula(){
    st_molekule = 0;
    read_state = other_block;
}

void Molekula::odpri (string datoteka) {
	podatki.open ( datoteka.c_str () );
}


// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(isspace))));
        return s;
}
// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}

bool Molekula::commonBeriBio (int wNum) {

	if (vrstica.find("MOLECULE") != string::npos) {
  		ime = vrstica.substr(vrstica.find("MOLECULE")+9);
		hetatms.clear();
		++st_molekule;
		if (wNum == -1 && st_molekule > 1 || first == false)
            return false;
	    if (wNum != -1)
	    	first=false;
	}
			
	else if (vrstica.find("HETATM") != string::npos) {

		std::vector<double> coord;
		string name = vrstica.substr(6, 6);
		trim(name);

		double x = stod(vrstica.substr(31, 8));
		coord.push_back(x);
		double y = stod(vrstica.substr(39, 8));
		coord.push_back(y);
		double z = stod(vrstica.substr(47, 8));
		coord.push_back(z);
	    
	    hetatms[name] = coord;
	}

	else if (vrstica.find("RIGID") != string::npos) {

		string buf;
	    stringstream ss(vrstica);
	    vector<double> sredina {0,0,0};
	    string name;

	    int i = 0;
	    while (ss >> buf) {
	    	if (i == 3) {
	    		name = buf;
	    	}
	    	else if (i > 3) {
	    		string atomType = buf.replace(buf.begin(),buf.begin()+1, "");
	    		std::vector<double> cord = hetatms.find(atomType)->second;
	    		sredina.at(0) += cord.at(0);
	    		sredina.at(1) += cord.at(1);
	    		sredina.at(2) += cord.at(2);
	    	}			    	
	    	i++;
	    }

	    //cout << name << " " << sredina.at(0)/(i-4) << " " << sredina.at(1)/(i-4) << " " << sredina.at(2)/(i-4) << endl;
	    v.push_back(Tocka (v.size(), name, sredina.at(0)/(i-4), sredina.at(1)/(i-4), sredina.at(2)/(i-4), name));

	}

	return true;

}

bool Molekula::commonBeri (int wNum, std::string &mol2string, std::vector<int> &hNumbers) {

    int st_atoma;
	char ime_atoma [10];
	char tip_atoma [10];
	float x, y, z;

	if ( wNum != -1 && vrstica != "@<TRIPOS>MOLECULE" && wNum != 0 ){
            mol2string+=vrstica+"\n";
    }

    if ( vrstica == "@<TRIPOS>MOLECULE" ) {
        read_state = name_block;
        ++st_molekule;
        if (wNum == -1 && st_molekule > 1 || first == false)
            return false;
        if (wNum != -1)
            first=false;
    }
    else if (read_state == name_block) {
        ime = vrstica;
        if (wNum != -1) {
            ime.erase(remove(ime.begin(), ime.end(), ' '), ime.end());
            if (ime == "") {
                ime = "Target-"+to_string(st_empty);
                st_empty++;
            }
        }
        read_state = other_block;
    }
    else if (vrstica.compare (0, 13, "@<TRIPOS>ATOM") == 0) {
        read_state = atom_block;
    }
    else if (vrstica.compare (0, 13, "@<TRIPOS>BOND") == 0) {
        read_state = bond_block;
    }
    else {
        if (read_state == atom_block) {
            sscanf (vrstica.c_str(), "%d %s %f %f %f %s %*d %*s %*f", &st_atoma, ime_atoma, &x, &y, &z, tip_atoma );
            if (*tip_atoma == 'H' && vodiki == false) {
                hNumbers.push_back(st_atoma);
            }
            else {
                v.push_back(Tocka (st_atoma,ime_atoma,x,y,z,tip_atoma));
            }
        }
        else if  (read_state == bond_block) {
            char temp_str2 [10];
            int a, b;
            if (sscanf (vrstica.c_str(), "%*d %d %d %s", &a, &b, temp_str2) != 3)
                return true;
            if ( vodiki == false && (find(hNumbers.begin(), hNumbers.end(),a)!=hNumbers.end() || find(hNumbers.begin(), hNumbers.end(),b)!=hNumbers.end() ) )
                return true;
            Vez povezava;

            for (int i = 0; i < (signed)v.size (); i++) {
                if (v[i].get_st_atoma () == a) {
                    povezava.atom_1 = i;
                }
                if (v[i].get_st_atoma () == b) {
                    povezava.atom_2 = i;
                }
            }
            vezi.push_back(povezava);
        }
    }

    return true;

}

std::string Molekula::beriZip (int wNum, std::istream &instream) {

    std::vector<int> hNumbers;
    string mol2string="";
    if (wNum != 0) {
        mol2string = "@<TRIPOS>MOLECULE\n";
    }
	bool status;
	initRead();

	if (!instream)
		return "0";
	while (instream) {
		getline(instream, vrstica);

		if (bioisoster) {
			status = commonBeriBio(wNum);
		}
		else {
			status = commonBeri(wNum, mol2string, hNumbers);
		}

		if (status == false)
            break;
	}

	naredi_vezavno_matriko ();
	naredi_dolzinsko_matriko ();

	return mol2string;
}

std::string Molekula::beri (int wNum) {

	std::vector<int> hNumbers;
    string mol2string="";
    if (wNum != 0) {
        mol2string = "@<TRIPOS>MOLECULE\n";
    }
	bool status;
	initRead();

	if (!infile)
		return "0";
	while (infile) {
		getline(infile, vrstica);

		if (bioisoster) {
			status = commonBeriBio(wNum);
		}
		else {
			status = commonBeri(wNum, mol2string, hNumbers);
		}

		if (status == false)
            break;
	}

	naredi_vezavno_matriko ();
	naredi_dolzinsko_matriko ();

	return mol2string;
}

bool Molekula::beriMol1 () {

	string mol2string="";
    vector<int> hNumbers;
    bool status;

    initRead();

	if (!podatki)
		return false;
	while (podatki) {
		getline(podatki, vrstica);

		if (bioisoster) {
			status = commonBeriBio(-1);
		}
		else {
			status = commonBeri(-1, mol2string, hNumbers);
		}

		if (status == false)
            break;

	}
	naredi_vezavno_matriko ();
	naredi_dolzinsko_matriko ();
	return true;
}

void Molekula::initRead() {
    vezi.clear();
	vezavna_matrika.clear();
	dolzinska_matrika.clear();
	v.clear();
}

Sredine Molekula::ar_sredina () const {
	Sredine s;
	float vsota_x = 0;
	float vsota_y = 0;
	float vsota_z = 0;
	for (int i = 0; i < (signed)v.size (); i++) {
		vsota_x += v[i].get_x ();
		vsota_y += v[i].get_y ();
		vsota_z += v[i].get_z ();
	}
	s.x = vsota_x / v.size ();
	s.y = vsota_y / v.size ();
	s.z = vsota_z / v.size ();
	return s;
}


Sredine Molekula::tezisce () const {
	Sredine gs;
	string element;
	float trenutna_vsota_x = 0;
	float trenutna_vsota_y = 0;
	float trenutna_vsota_z = 0;
	float masa = 0;
	for (int i = 0; i < (signed)v.size (); i++) {
		element = v[i].get_tip_atoma ().substr (0,1);
		if (element == "C") {
			trenutna_vsota_x = trenutna_vsota_x + (mC * v[i].get_x ());
			trenutna_vsota_y = trenutna_vsota_y + (mC * v[i].get_y ());
			trenutna_vsota_z = trenutna_vsota_z + (mC * v[i].get_z ());
			masa = masa + mC;
		}
		if (element == "O") {
			trenutna_vsota_x = trenutna_vsota_x + (mO * v[i].get_x ());
			trenutna_vsota_y = trenutna_vsota_y + (mO * v[i].get_y ());
			trenutna_vsota_z = trenutna_vsota_z + (mO * v[i].get_z ());
			masa = masa + mO;
		}
		if (element == "H") {
			trenutna_vsota_x = trenutna_vsota_x + (mH * v[i].get_x ());
			trenutna_vsota_y = trenutna_vsota_y + (mH * v[i].get_y ());
			trenutna_vsota_z = trenutna_vsota_z + (mH * v[i].get_z ());
			masa = masa + mH;
		}
		if (element == "N") {
			trenutna_vsota_x = trenutna_vsota_x + (mN * v[i].get_x ());
			trenutna_vsota_y = trenutna_vsota_y + (mN * v[i].get_y ());
			trenutna_vsota_z = trenutna_vsota_z + (mN * v[i].get_z ());
			masa = masa + mN;
		}
		if (element == "S") {
			trenutna_vsota_x = trenutna_vsota_x + (mS * v[i].get_x ());
			trenutna_vsota_y = trenutna_vsota_y + (mS * v[i].get_y ());
			trenutna_vsota_z = trenutna_vsota_z + (mS * v[i].get_z ());
			masa = masa + mS;
		}
		if (element == "P") {
			trenutna_vsota_x = trenutna_vsota_x + (mP * v[i].get_x ());
			trenutna_vsota_y = trenutna_vsota_y + (mP * v[i].get_y ());
			trenutna_vsota_z = trenutna_vsota_z + (mP * v[i].get_z ());
			masa = masa + mP;
		}
		if (element == "F") {
			trenutna_vsota_x = trenutna_vsota_x + (mF * v[i].get_x ());
			trenutna_vsota_y = trenutna_vsota_y + (mF * v[i].get_y ());
			trenutna_vsota_z = trenutna_vsota_z + (mF * v[i].get_z ());
			masa = masa + mF;
		}
		if (element == "Cl") {
			trenutna_vsota_x = trenutna_vsota_x + (mCl * v[i].get_x ());
			trenutna_vsota_y = trenutna_vsota_y + (mCl * v[i].get_y ());
			trenutna_vsota_z = trenutna_vsota_z + (mCl * v[i].get_z ());
			masa = masa + mCl;
		}
		if (element == "Br") {
			trenutna_vsota_x = trenutna_vsota_x + (mBr * v[i].get_x ());
			trenutna_vsota_y = trenutna_vsota_y + (mBr * v[i].get_y ());
			trenutna_vsota_z = trenutna_vsota_z + (mBr * v[i].get_z ());
			masa = masa + mBr;
		}
		if (element == "I") {
			trenutna_vsota_x = trenutna_vsota_x + (mI * v[i].get_x ());
			trenutna_vsota_y = trenutna_vsota_y + (mI * v[i].get_y ());
			trenutna_vsota_z = trenutna_vsota_z + (mI * v[i].get_z ());
			masa = masa + mI;
		}
	}
	gs.x = (float) (trenutna_vsota_x / masa);
	gs.y = (float) (trenutna_vsota_y / masa);
	gs.z = (float) (trenutna_vsota_z / masa);
	return gs;
}


Tocka Molekula::dobi_atom (int st_atoma) const {
	return v [st_atoma];
}

size_t Molekula::velikost_molekule () const {
	return v.size();
}

const string &Molekula::get_ime() const {
	return ime;
}

const vector <Vez> &Molekula::get_vezi () const {
	return vezi;
}

void Molekula::naredi_vezavno_matriko () {
	vezavna_matrika.resize (v.size (), vector <bool> (v.size (), false));
	for (auto &i : get_vezi() ) {
		vezavna_matrika [i.atom_1] [i.atom_2] = true;
		vezavna_matrika [i.atom_2] [i.atom_1] = true;
	}
}

const vector <vector <bool> > &Molekula::get_vezavna_matrika () const{
	return vezavna_matrika;
}


void Molekula::naredi_dolzinsko_matriko () {
	dolzinska_matrika.resize (v.size (), vector <int> (v.size(), 0));
	for (int i = 0; i < (signed)vezavna_matrika.size(); i++) {
		for (int j = 0; j < (signed)vezavna_matrika.size(); j++) {
			if (vezavna_matrika [i] [j] == false && i != j) {
				dolzinska_matrika [i] [j] = numeric_limits<int>::max();
			}
			else if (vezavna_matrika [i] [j] == true) {
				dolzinska_matrika [i] [j] = 1;
			}
		}
	}
	//FW-algoritem:
	for (int k = 0; k < (signed)dolzinska_matrika.size(); k++) {
		for (int i = 0; i < (signed)dolzinska_matrika.size(); i++) {
			for (int j = 0; j < (signed)dolzinska_matrika.size(); j++) {
				if (dolzinska_matrika[i][k] == numeric_limits<int>::max() || dolzinska_matrika[k][j] == numeric_limits<int>::max())
					continue;
				if (dolzinska_matrika[i][j] > dolzinska_matrika[i][k] + dolzinska_matrika[k][j]) {
					dolzinska_matrika[i][j] = dolzinska_matrika[i][k] + dolzinska_matrika[k][j];
				}
			}
		}
	}
}

const vector <vector <int> > &Molekula::get_dolzinska_matrika () const{
	return dolzinska_matrika;
}

ostream& operator<<(ostream& s, const Tocka& t) {
	s << t.get_x() << "		" << t.get_y() << " 	" << t.get_z() << "		"<< t.get_ime_atoma() << "	" << t.get_tip_atoma();
	return s;
}


void Molekula::najdi_ujemanje_seedov(string seedsFile) {

	for (int i = 0; i < v.size(); i++) {
		vector<string> tmp;
		complementarySeeds[v.at(i).get_tip_atoma()] = tmp;
	}

	ifstream file(seedsFile);
    string line, name1, name2, seed1, seed2;
    double HD, something;

    while (getline(file, line)) {

    	istringstream ss(line);
		ss >> HD >> something >> name1 >> name2 >> seed1 >> seed2;

		if ( HD > 1 ) {
			dodaj_ujemanje_seedov(seed1, seed2);
        }
    }

}


void Molekula::najdi_dodatno_ujemanje_seedov(string seedsFile) {

	ifstream file(seedsFile);
    string line, seed1, seed2;

    while (getline(file, line)) {

    	istringstream ss(line);
		ss >> seed1 >> seed2;
		dodaj_ujemanje_seedov(seed1, seed2);
        
    }

}


// doda samo tiste kombinacije ki vsebujejo seed ki se nahaja v referencni molekuli
void Molekula::dodaj_ujemanje_seedov(string seed1, string seed2) {

	if (complementarySeeds.find(seed1) != complementarySeeds.end()) {
		complementarySeeds[seed1].push_back(seed2);
	}
		
	if (complementarySeeds.find(seed2)!= complementarySeeds.end()) {
		complementarySeeds[seed2].push_back(seed1);
	}

}