#include <iostream>
#include <vector>
#include <string>
#include <sys/stat.h>
#include <assert.h>

#ifdef _WIN32
#include <ctime>
#include <thread>
#else
#include <unistd.h>
#endif

#include "inout.hpp"

using namespace std;

bool Inout::pluginGlobal = false;

inline bool existsTest(const std::string& name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

//
// set default parameters
//
Inout::Inout(void) {

    num = 0;
    stikalo_3D = "2D";
    dim = 2;
    s = 1;
    maxr = 1;
    wNum = 0;
    nc = 1;
    resultsFile = "lisica_results.txt";
    vodiki = false;
    stikalo_maxr = false;
    stikalo_s = false;
    resultsDir = "results";
    bioisoster = false;
    seedsFile = "";
    seedsFile2 = "";

}

//
// create timestamp for results directory
//

string Inout::createTimeStamp(){

    time_t ltime;
    struct tm *Tm;

    ltime=time(NULL);
    Tm=localtime(&ltime);

    timestamp = to_string(Tm->tm_mday) + to_string(Tm->tm_mon + 1) + to_string(Tm->tm_year) + to_string(Tm->tm_hour) + to_string(Tm->tm_min) + to_string(Tm->tm_sec);

    return timestamp;

}

//
// print out error and exit
//
void Inout::printOut(string what, string parameter = NULL){

    if(what == "help") {
        cout << "\nUse: " << parameter << " -R <path to reference molecule> -T <path to target molecules> [parameters]\n\nParameters:\n-n <the number of CPUs to use> \n "
            "Default value: the default is to try to detect the number of CPUs or, failing that, use 1 \n\n-d <product graph dimension>\n Possible input: 2, 3 or b.\n "
            "Default value: 2\n\n-m <maximum allowed atom spatial distance for 3D product graph>\n Default value: 1\n\n-s <maximum allowed shortest path size for 2D "
            "product graph>\n Default value: 1\n"
            "\n-w <number of highest ranked molecules to write to output >\n Default value: 0\n\n"
            "-c <maximum allowed number of highest scoring conformations to be outputed>\n Deafult value: 1\n\n"
            "-h <consider hydrogens>\nDeafult value: false\n\n"
            "-f <results directory>\nDeafult value: results\n" << endl;
    }

    else if(what == "error") {
        cout << "\nInvalid input.\n\nUse: " << parameter << " -R <path to reference molecule> -T <path to target molecules> [parameters]\n\nSee: " << parameter << " --help.\n" << endl;
    }

    else if(what == "error2") {
        cout << "\nInvalid input: \n\n"
        "Input for path to reference molecule is obligatory: -R <path to reference molecule>\n"
        "Input for path to target molecules is obligatory: -T <path to target molecules>\n"
        "\n\nUse: " << parameter << " -R <path to reference molecule> -T <path to target molecules> [parameters]\n\n"
        "See: " << parameter << " --help\n" << endl;
    }

    else if (what == "wrongDim") {
        cout << "\nInvalid input.\n\nPossible input for product graph dimension are:\n-d 2\n-d 3\n-d b\n\nSee: " << parameter <<" --help.\n" << endl;
    }

    else if (what == "nofile") {
        cout << "\nInvalid input.\n\nCan't find file: " << parameter <<".\n" << endl;
    }

    exit(0);

}

void Inout::checkInputParameters(vector<string> argv) {

        int st = 0;

        // get input parameters
       for(unsigned i=1; i<argv.size(); i++) {

	        if (argv.at(i) == "--plugin") {
	            pluginGlobal = true;
	        }
            else if (argv.at(i) == "--help") {
                Inout::printOut("help", argv.at(0));
            }
            else if (argv.at(i) == "-R") {
                st++;
                i++;
                refMol = argv.at(i);
                if (existsTest(refMol) == false) {
                    Inout::printOut("nofile", refMol);
                }
            }
            else if (argv.at(i) == "-T") {
                st++;
                i++;
                tarMol = argv.at(i);
                 if (existsTest(tarMol) == false) {
                    Inout::printOut("nofile", tarMol);
                }
            }

            else if (argv.at(i) == "-sf") {
                i++;
                seedsFile = argv.at(i);
                if (existsTest(seedsFile) == false) {
                    Inout::printOut("nofile", seedsFile);
                }
            }

            else if (argv.at(i) == "-sf2") {
                i++;
                seedsFile2 = argv.at(i);
                if (existsTest(seedsFile2) == false) {
                    Inout::printOut("nofile", seedsFile2);
                }
            }

            else if (argv.at(i) == "-d") {
                i++;

                if (argv.at(i) == "b") {
                    bioisoster = true;
                    stikalo_3D = "bio";
                }
                else {
                    dim=atoi((argv.at(i)).c_str());
                    if(dim !=2 && dim !=3){
                        Inout::printOut("wrongDim", argv.at(0));
                    }
                    else if (dim == 3){
                        stikalo_3D = "3D";
                    }
                }
            }
            else if (argv.at(i) == "-m"){
                i++;
                maxr = atof((argv.at(i)).c_str());
                stikalo_maxr = true;
                if (maxr <= 0) {
                    cout << "\n\nInvalid input: Maximum allowed atom spatial distance for 3D product graph (-m) has to be greater than zero.\n" << endl;
                    exit(0);
                }
            }
            else if (argv.at(i) == "-s"){
                i++;
                s = atoi((argv.at(i)).c_str());
                stikalo_s = true;
                if (s < 0) {
                    cout << "\n\nInvalid input: Maximum allowed shortest path size for 2D product graph (-s) has to be greater than zero.\n" << endl;
                    exit(0);
                }
            }
            else if (argv.at(i) == "-w"){
                i++;
                wNum = atof((argv.at(i)).c_str());
                if (wNum < 0) {
                    cout << "\n\nInvalid input: Number of highest ranked molecules to write to output (-w) has to be a positive number.\n" << endl;
                    exit(0);
                }
            }
            else if (argv.at(i) == "-c"){
                i++;
                nc = atof((argv.at(i)).c_str());
                if (nc <= 0) {
                    cout << "\n\nInvalid input: Maximum allowed number of highest scoring conformations to be outputed (-c) has to be greater than zero.\n" << endl;
                    exit(0);
                }
            }
            else if (argv.at(i) == "-n"){
                i++;
                num = atof((argv.at(i)).c_str());
                if (num <= 0) {
                    cout << "\n\nInvalid input: The number of CPUs to use (-n) has to be greater than zero.\n" << endl;
                    exit(0);
                }
            }
            else if (argv.at(i) == "-h"){
                vodiki = true;
            }
            else if (argv.at(i) == "-f"){
                i++;
                resultsDir = argv.at(i);
            }
            else {
                Inout::printOut("error", argv.at(0));
            }

    }

        // extra check
    if (st != 2) {
        Inout::printOut("error2", argv.at(0));
    }

    if (stikalo_3D == "bio" && !existsTest(seedsFile)) {
        cout << "\n\nInvalid input: For bio dimension calculation seeds file is required. Please define the path to seeds file (-sf file).\n" << endl;
        exit(0);
    }

    Inout::dodatniCheck();
	if (wNum > 0) {
		timestamp = Inout::createTimeStamp();
	}

        //set number of cores
#ifdef _WIN32
	 if (num == 0 || (num > std::thread::hardware_concurrency() && std::thread::hardware_concurrency() != 0)) {
		 num = std::thread::hardware_concurrency();
	 }
#else
	if(num==0 || (num > sysconf(_SC_NPROCESSORS_ONLN) && sysconf(_SC_NPROCESSORS_ONLN) != 1  )) {
        num = sysconf(_SC_NPROCESSORS_ONLN);
	}
#endif


}


void Inout::dodatniCheck() {

    char response;
    if (stikalo_3D != "3D" && stikalo_maxr == true){
        cout << "\nWarning: -m parameter is maximum allowed atom spatial distance for 3D product graph. However you chose the " << stikalo_3D << " option." << endl;
        while(true) {
            cout << "\nContinue anyway? (y or n): ";
            cin >> response;
            if (response == 'n'){
                exit(0);
            }
            else if (response == 'y'){
                break;
            }
        }
    }

    if (stikalo_3D != "2D" && stikalo_s == true){
        cout << "\nWarning: -s parameter is maximum allowed shortest path size for 2D product graph. However you chose the " << stikalo_3D << " option." << endl;
        while(true) {
            cout << "\nContinue anyway? (y or n): ";
            cin >> response;
            if (response == 'n'){
                exit(0);
            }
            else if (response == 'y'){
                break;
            }
        }
    }

    if (wNum == 0 && nc > 1){
        cout << "\nWarning: -c parameter is the maximum allowed number of highest scoring conformations to be outputed. You did not chose to output the common structure files." << endl;
        while(true) {
            cout << "\nContinue anyway? (y or n): ";
            cin >> response;
            if (response == 'n'){
                exit(0);
            }
            else if (response == 'y'){
                break;
            }
        }
    }

}


