#ifndef PRODUKTNI_GRAF_HPP
#define PRODUKTNI_GRAF_HPP

#include <vector>
#include "molekula.hpp"


class Produktni_graf {
	private:
		void naredi_produktni_graf (const Molekula&, const Molekula&);
		void naredi_produktni_graf_3D (const Molekula&, const Molekula&, bool bioisostere);
		std::vector <Komponente> vozlisca2;
		void naredi_pMatriko ();

	public:
		Produktni_graf(const Molekula &m1, const Molekula &m2, std::string tride);
		~Produktni_graf();
		const std::vector <Komponente> &get_vozlisca () const;
		const std::vector <std::vector <bool> > &get_pMatrika () const;
	    std::string primerjaj (const Molekula &m1, const Molekula &m2, int writeMol);
	    void ovrednoti ();
	    const int dobi_velikost_PG ();
	    void set_s(double s);
	    void set_m (double m);
	    std::vector <Komponente> vozlisca;
        std::vector <std::vector <bool> > pMatrika;
        int sz;
        bool **vrstice;
        int *qmax;
	    int qsize;



};
#endif
