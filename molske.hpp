#ifndef MOLSKE
#define MOLSKE

#define mC 12.0107
#define mH 1.0079
#define mO 15.9994
#define mN 14.0067
#define mS 32.065
#define mP 30.9738
#define mF 18.9984
#define mCl 35.453
#define mBr 79.904
#define mI 126.9045

#endif
