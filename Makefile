SRC = main.cpp molekula.cpp produktni_graf.cpp mcqd.cpp inout.cpp
DST = lisica
CPPFLAGS_ALWAYS=-static -Wl,--whole-archive -lpthread -Wl,--no-whole-archive -std=c++0x
INCL =

ifdef DEBUG
	EXTRAFLAGS=-g
else
	EXTRAFLAGS=-O3 -DNDEBUG
endif
	
CPPFLAGS = $(INCL) $(EXTRAFLAGS) $(CPPFLAGS_ALWAYS)

OBJS = $(SRC:.cpp=.o)

$(DST): $(OBJS)
	g++ $(CPPFLAGS) -lpthread -o $(DST) $(OBJS) -lboost_iostreams -lz

.PHONY: debug
debug:
	$(MAKE) DEBUG=1

.PHONY: clean
clean:
	rm -f $(OBJS) $(DST)
