#ifndef INOUT_HPP
#define INOUT_HPP

#include <string>
#include <vector>

class Inout {
	public:
        Inout();
		void checkInputParameters(std::vector<std::string> argv);
		static bool pluginGlobal;
        bool stikalo_maxr, stikalo_s, vodiki, bioisoster;
        int num, dim, wNum, nc;
        double s, maxr;
        std::string resultsFile, refMol, tarMol, resultsDir, timestamp, stikalo_3D, seedsFile, seedsFile2;

		static bool getPlugin() { return pluginGlobal; }

    private:
        void printOut(std::string what, std::string parameter);
        void dodatniCheck();
        std::string createTimeStamp();

};


#endif
