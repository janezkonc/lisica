#include <iostream>
#include <cstring>
#include <iomanip>
#include <mutex>
#include <unordered_map>
#include <algorithm>
#include <thread>
#include <memory>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>

#include <fstream>

#ifdef _WIN32
#include <direct.h>
#else
#include <getopt.h>
#endif

#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>


#include "debug.hpp"
#include "molekula.hpp"
#include "produktni_graf.hpp"
#include "inout.hpp"
using namespace std;


Molekula m1;
Inout inout;

struct bestMolekula {
    string mol2;
    string name;
    float tan;
};

vector <Molekula*> mol;
typedef Produktni_graf* graf_1;
graf_1 graf_tmp;
vector <Produktni_graf*> graf;
vector <float> T;

vector <bestMolekula> bestMolekule;
vector <bestMolekula> sortedBestMol;

mutex mtx, mtx2;
ifstream infile;

unordered_map<string, float> hashMap;
vector<pair<float,string>> items;

bool vodiki, bioisoster;
double maxr, s;

bool gzFile = false;

class global_istream : public std::istream {
    public:
#ifdef _WIN32
	global_istream() : std::istream(nullptr) {};
#else
	global_istream() : std::istream() {};
#endif
};

global_istream instream;

void create_vectors (int num_p){

	for (int i=0; i<num_p; i++){
		float T_tmp;
		T.push_back(T_tmp);
		mol.push_back(new Molekula());
		graf.push_back(graf_tmp);
		bestMolekule.push_back(bestMolekula());
	}

}


void insert_map (float t, string name){
    unordered_map<string, float>::iterator it = hashMap.find(name);
    if (it != hashMap.end()) {
        if(it->second < t) {
            it->second = t;
        }
    }
    else {
        hashMap.insert(make_pair(name, t ));
    }
}


void sort_map(){
    pair<float, string> tmp;
    for (auto& x: hashMap) {
        tmp = make_pair(x.second, x.first);
        items.push_back(tmp);
    }
    sort(items.begin(), items.end());
}

void print_sorted (){
    if (Inout::getPlugin()) {
        ofstream myfile;
        myfile.open(inout.resultsFile);
        for (unsigned i = items.size(); i > 0; i--){
            myfile << items[i - 1].first << " " << items[i - 1].second << "\n";
        }
        myfile.close();
    }
    else {
        for (unsigned i=items.size(); i>0; i--){
            cout << items[i-1].first << " " << items[i-1].second << endl;
        }
    }
}

void print_write(){

	ofstream myfile;
	struct stat st = { 0 };

#ifdef _WIN32
        if (stat((inout.resultsDir).c_str(), &st) == -1) {
            _mkdir((inout.resultsDir).c_str());
        }
        _mkdir((inout.resultsDir + "/" + inout.timestamp).c_str());
#else
        if (stat((inout.resultsDir).c_str(), &st) == -1) {
            mkdir((inout.resultsDir).c_str(), 0700);
        }
        mkdir((inout.resultsDir + "/" + inout.timestamp).c_str(), 0700);
#endif

    if (Inout::getPlugin()) {
        ofstream myfile2;
        myfile2.open(inout.resultsDir + "/" + inout.timestamp + "/lisica_results.txt");
        for (int i = 0; i < sortedBestMol.size(); i++) {
            myfile2 << sortedBestMol.at(i).tan << " " << sortedBestMol.at(i).name << "\n";
            if (i < inout.wNum) {
                myfile.open(inout.resultsDir + "/" + inout.timestamp + "/" + to_string(i + 1) + "_" + sortedBestMol.at(i).name + ".mol2");
                myfile << sortedBestMol.at(i).mol2 << "\n" << "Score: " << sortedBestMol.at(i).tan;
                myfile.close();
            }
        }
    }
    else {
        for (int i = 0; i<sortedBestMol.size(); i++) {
            cout << sortedBestMol.at(i).tan << " " << sortedBestMol.at(i).name << endl;
            myfile.open(inout.resultsDir+"/"+inout.timestamp + "/" + to_string(i + 1) + "_" + sortedBestMol.at(i).name + ".mol2");
            myfile << sortedBestMol.at(i).mol2 << "\n" << "Score: " << sortedBestMol.at(i).tan;
            myfile.close();
        }

        cout << "\nFiles with common structures are available in "+inout.resultsDir+"/"+inout.timestamp+" directory." << endl;
    }
}

bool compareByTanimoto(const bestMolekula &a, const bestMolekula &b) {
    return a.tan > b.tan;
}

void insert_mapWrite (bestMolekula tmpMol){

    int konformacije = 0;
    int idx = -1;
    int i = 0;

    for(; i < sortedBestMol.size(); i++){
        if (sortedBestMol.at(i).name == tmpMol.name){
            konformacije++;
        }
        if (konformacije == inout.nc) {
            idx = i;
            break;
        }
    }

    if (i == sortedBestMol.size() && sortedBestMol.size() < inout.wNum) {
        sortedBestMol.push_back(tmpMol);
    }
    else if ( sortedBestMol.at(sortedBestMol.size()-1).tan < tmpMol.tan ) {
        if (idx == -1) {
            sortedBestMol.at(sortedBestMol.size()-1) = tmpMol;
        }
        else if (sortedBestMol.at(idx).tan < tmpMol.tan ) {
            sortedBestMol.at(idx) = tmpMol;
        }
    }
    sort(sortedBestMol.begin(), sortedBestMol.end(), compareByTanimoto);

}


void parallelFun(int id_p){

    while (true){
        mtx.lock();

        string check = "";

        if (gzFile) {
            check = mol.at(id_p)->beriZip(inout.wNum, instream);
        }
        else {
            check = mol.at(id_p)->beri(inout.wNum);
        }

        if (inout.wNum == 0) {
            if(check=="0"){mtx.unlock(); break; }
        }
        else {
            if(check=="0"){
                mtx.unlock();
                break;
            }
            else {
                bestMolekule.at(id_p).mol2 = check;
            }
        }

        mtx.unlock();
        graf.at(id_p) = new Produktni_graf(m1, *mol.at(id_p), inout.stikalo_3D);
		dbgmsg("velikost produktnega grafa = " << (graf.at(id_p))->dobi_velikost_PG());


		if ((graf.at(id_p))->dobi_velikost_PG() == 0) {
			T.at(id_p) = 0.000000;
		}
		else {
			dbgmsg("pred primerjaj");
			if (inout.wNum == 0) {
                graf.at(id_p)->primerjaj(m1, *mol.at(id_p), 0);
			}
			else {
                bestMolekule.at(id_p).mol2 += graf.at(id_p)->primerjaj(m1, *mol.at(id_p), 1);
			}
			T.at(id_p) = (double) (graf.at(id_p)->qsize) / ( (double) (m1.velikost_molekule()) + (double) (mol.at(id_p)->velikost_molekule()) - (double) (graf.at(id_p)->qsize));
			dbgmsg("za primerjaj T = " << T.at(id_p));
		}

		if (inout.wNum!=0){
            bestMolekule.at(id_p).name = mol.at(id_p)->get_ime();
            bestMolekule.at(id_p).tan = T.at(id_p);
            mtx2.lock();
                insert_mapWrite(bestMolekule.at(id_p));
            mtx2.unlock();
		}
		else {
            mtx2.lock();
                insert_map(T.at(id_p), mol.at(id_p)->get_ime());
            mtx2.unlock();
        }

	        delete graf.at(id_p);

    }
    delete mol.at(id_p);

}

int main (int argc, char *argv[]) {

	vector<string> all_args;
	all_args.assign(argv, argv + argc);

    inout.checkInputParameters(all_args);

    vodiki = inout.vodiki;
    maxr = inout.maxr;
    s = inout.s;
    bioisoster = inout.bioisoster;
    create_vectors(inout.num);



    m1.odpri(inout.refMol);
    m1.beriMol1();

    if (bioisoster) {
        m1.najdi_ujemanje_seedov(inout.seedsFile);
    }
    if (inout.seedsFile2 != "") {
        m1.najdi_dodatno_ujemanje_seedov(inout.seedsFile2);
    }

    // input is gz file
    if(inout.tarMol.substr(inout.tarMol.find_last_of('.')+1, inout.tarMol.length()) == "gz") {

        gzFile = true;

        std::ifstream file(inout.tarMol, std::ios_base::in | std::ios_base::binary);
        boost::iostreams::filtering_streambuf<boost::iostreams::input> inbuf;
        inbuf.push(boost::iostreams::gzip_decompressor());
        inbuf.push(file);

        instream.rdbuf(&inbuf);

        vector<thread> threads;
        dbgmsg("num threads = " << inout.num);
        for(int i = 0; i < inout.num; i++){
            threads.push_back(thread(parallelFun, i));
        }

        for(auto& thread : threads){
            thread.join();
        }

        file.close();

    }
    else {
        infile.open(inout.tarMol, ifstream::in);

        vector<thread> threads;
        dbgmsg("num threads = " << inout.num);
        for(int i = 0; i < inout.num; i++){
            threads.push_back(thread(parallelFun, i));
        }

        for(auto& thread : threads){
            thread.join();
        }

        infile.close();
    }


    if (inout.wNum > 0) {
        print_write();
    }
    else {
        sort_map();
        print_sorted();
    }

	// info will be written to done.txt
    ofstream myfile;
	myfile.open("done.txt");
	myfile << inout.timestamp + "\n";
	myfile.close();
}


