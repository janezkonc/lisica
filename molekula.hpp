#ifndef MOLEKULA_HPP
#define MOLEKULA_HPP


#include <string>
#include <vector>
#include <fstream>
#include <limits>
#include "molske.hpp"
#include <vector>
#include <map>
#include <utility>

class Tocka {
	private:
		int st_atoma;
		float x, y, z;
		std::string ime_atoma, tip_atoma;
	public:
		Tocka (): st_atoma (0), x(0), y(0), z(0) {}
		Tocka (int _st_atoma, std::string _ime_atoma, float _x, float _y, float _z,std::string _tip_atoma) :
			st_atoma (_st_atoma), x(_x), y(_y), z(_z),ime_atoma (_ime_atoma),tip_atoma(_tip_atoma) {}
		Tocka (int _st_atoma, float _x, float _y, float _z,std::string _tip_atoma) :
			st_atoma (_st_atoma), x(_x), y(_y), z(_z),tip_atoma(_tip_atoma) {}
		float get_x () const {
			return x;
		}
		float get_y () const {
			return y;
		}
		float get_z () const {
			return z;
		}
		int get_st_atoma () const {
			return st_atoma;
		}
		std::string get_ime_atoma () const {
			return ime_atoma;
		}
		std::string get_tip_atoma () const {
			return tip_atoma;
		}
};


class Sredine;

class Vez {
	public:
		int atom_1;
		int atom_2;
};

class Molekula {
	private:
		std::vector <Vez> vezi;
		std::ifstream podatki;
		void naredi_vezavno_matriko ();
		void naredi_dolzinsko_matriko ();
		std::vector <std::vector <bool> > vezavna_matrika;
		std::vector <std::vector <int> > dolzinska_matrika;
		int st_molekule, stAtoma;
		enum {atom_block, bond_block, other_block, name_block};
		std::string ime;
		std::map<std::string, std::vector<double>> hetatms;
		bool commonBeri (int wNum, std::string &mol2string, std::vector<int> &hNumbers);
		bool commonBeriBio (int wNum);
		void initRead();
		void dodaj_ujemanje_seedov(std::string seed1, std::string seed2);

	public:
		Molekula();
		std::vector <Tocka> v;
		std::string vrstica;
		void odpri (std::string);
		std::string beri (int wNum);
		std::string beriZip (int wNum, std::istream &in);
		bool beriMol1();
		bool beri_PDB(std::string);
		Sredine ar_sredina () const;
		Sredine tezisce () const;
		Tocka dobi_atom (int) const;
		size_t velikost_molekule() const;
		const std::vector <Vez> &get_vezi () const;
		const std::vector <std::vector <bool> > &get_vezavna_matrika () const;
		const std::vector <std::vector <int> > &get_dolzinska_matrika () const;
		const std::string &get_ime () const;
		std::map<std::string, std::vector<std::string>> complementarySeeds;
		void najdi_ujemanje_seedov(std::string seedsFile);
		void najdi_dodatno_ujemanje_seedov(std::string seedsFile);

};

class Sredine {
	public:
		float x;
		float y;
		float z;
};

class Komponente {
	public:
		int komponenta_1;
		int komponenta_2;
		std::vector <int> pVezi;
};
#endif
